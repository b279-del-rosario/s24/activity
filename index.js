// console.log("HELLO!");

//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
const getCube = Math.pow(2, 3);

// Template Literals
let message = "The cube of 2 is " + getCube;
console.log(message);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [number, street, state, postal] = address
console.log(`I live at ${number} ${street}, ${state} ${postal}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((reduceNumber) => {
	console.log(`${reduceNumber}`)
})

const add = (num1, num2, num3, num4, num5) => num1 + num2 + num3 + num4 + num5;
let total = add (1, 2, 3, 4, 5);
console.log(total);

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachschund";

console.log(myDog);




// Javascript Classes



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}